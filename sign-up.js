const password = document.getElementById("password");
const retypePassword = document.getElementById("retype-password");
const firstName = document.getElementById("first-name");
const lastName = document.getElementById("last-name");
const email = document.getElementById("email");
const tos = document.getElementById("tos");

function storageAvailable(type) {
  let storage;
  try {
    storage = window[type];
    const x = "__storage_test__";
    storage.setItem(x, x);
    storage.removeItem(x);
    return true;
  } catch (e) {
    return (
      e instanceof DOMException &&
      // everything except Firefox
      (e.code === 22 ||
        // Firefox
        e.code === 1014 ||
        // test name field too, because code might not be present
        // everything except Firefox
        e.name === "QuotaExceededError" ||
        // Firefox
        e.name === "NS_ERROR_DOM_QUOTA_REACHED") &&
      // acknowledge QuotaExceededError only if there's something already stored
      storage &&
      storage.length !== 0
    );
  }
}

let validPassword = false;
let validRetypePassword = false;
let validEmail = false;
let validName = false;
let validToS = false;

const setButtonState = () => {
  if (!allFieldsValid()) {
    document.getElementById("form-submit-button").style.background = "grey";
    document
      .getElementById("sign-up-form-section")
      .addEventListener("submit", (event) => {
        event.preventDefault();
      });
  } else {
    document.getElementById("form-submit-button").style.background =
      "var(--maximum-yellow-red)";
    document
      .getElementById("sign-up-form-section")
      .addEventListener("submit", submit);
  }
};

const validateRetypePassword = () => {
  let retypePasswordLabel = retypePassword.nextElementSibling;

  if (password.value !== retypePassword.value || password.value === "") {
    retypePasswordLabel.style.color = "red";
    validRetypePassword = false;
  } else {
    retypePasswordLabel.style.color = "green";
    validRetypePassword = true;
  }
  setButtonState();
};

const validatePassword = () => {
  let passwordLabel = password.nextElementSibling;

  if (password.value.length < 8) {
    passwordLabel.style.color = "red";
    validPassword = false;
  } else {
    passwordLabel.style.color = "green";
    validPassword = true;
  }
  validateRetypePassword();
  setButtonState();
};

const validateName = (field) => {
  const invalidCharacters = new RegExp(
    /[0-9`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/
  );

  let nameLabel = field.nextElementSibling;

  if (field.value.trim() === "") {
    nameLabel.style.color = "red";
    field.value = "";
    validName = false;
  } else if (invalidCharacters.test(field.value)) {
    nameLabel.style.color = "red";
    validName = false;
  } else {
    nameLabel.style.color = "green";
    validName = true;
  }

  setButtonState();
};

const validateEmail = () => {
  const validEmailCharacters = new RegExp(
    /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
  ); // RFC2822 Email Validation RegEx

  let emailLabel = email.nextElementSibling;

  if (!validEmailCharacters.test(email.value.trim())) {
    emailLabel.style.color = "red";
    validEmail = false;
  } else {
    emailLabel.style.color = "green";
    validEmail = true;
  }

  setButtonState();
};

const validateToS = () => {
  validToS = tos.checked;
  setButtonState();
};

password.onkeyup = validatePassword;
retypePassword.onkeyup = validateRetypePassword;
firstName.onkeyup = () => validateName(firstName);
lastName.onkeyup = () => validateName(lastName);
email.onkeyup = validateEmail;
tos.onclick = validateToS;

const allFieldsValid = () => {
  return (
    validEmail && validName && validPassword && validRetypePassword && validToS
  );
};

const successfullySubmitted = () => {
  let element = document.createElement("h1");
  element.style.display = "flex";
  element.style.alignSelf = "center";
  element.style.textAlign = "center";
  element.innerText = "You have successfully submitted the form.";

  return element;
};

const returnToHomePage = () => {
  let element = document.createElement("a");
  element.href = "/";
  element.style.display = "flex";
  element.style.alignSelf = "center";
  element.innerText = "Return to homepage";
  element.style.textDecoration = "underline";

  return element;
};

const submit = (event) => {
  event.preventDefault();

  if (allFieldsValid()) {
    if (storageAvailable("localStorage")) {
      localStorage.firstSignUp = false;
      localStorage.firstName = firstName.value.trim();
      localStorage.lastName = lastName.value.trim();
      localStorage.email = email.value.trim();
      localStorage.tos = tos.checked;
    } else {
      document.getElementById("could-not-submit-text").style.display = "block";
    }
    document.getElementById("sign-up-form").remove();
    document.getElementById("already-submitted-text").remove();
    document.getElementById("sign-up-text").remove();
    document
      .getElementById("sign-up-form-section")
      .appendChild(successfullySubmitted());
    document
      .getElementById("sign-up-form-section")
      .appendChild(returnToHomePage());
  }
};

(() => {
  document
    .getElementById("sign-up-form-section")
    .addEventListener("submit", submit);
  if (storageAvailable("localStorage")) {
    document.getElementById("first-name").value = localStorage.firstName || "";
    document.getElementById("last-name").value = localStorage.lastName || "";
    document.getElementById("email").value = localStorage.email || "";
    document.getElementById("tos").checked = localStorage.tos;

    validateName(firstName);
    validateName(lastName);
    validateEmail();
    validatePassword();
    validateRetypePassword();
    validateToS();

    if (localStorage.firstSignUp === "false") {
      document
        .getElementById("form-submit-button")
        .setAttribute("value", "Update Details");
    }
  }

  if (
    localStorage.firstName !== undefined &&
    localStorage.lastName !== undefined &&
    localStorage.email !== undefined &&
    localStorage.tos !== false
  ) {
    document
      .getElementById("already-submitted-text")
      .setAttribute("style", "display:block");
  }
})();
