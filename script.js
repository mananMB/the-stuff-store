const productsEndpoint = "https://fakestoreapi.com/products";
const categoriesEndpoint = "https://fakestoreapi.com/products/categories";

const fetchData = (endpoint) =>
  fetch(endpoint).then((response) => response.json());

const loaderTemplate = () => {
  let loaderContainer = document.createElement("div");
  loaderContainer.setAttribute("class", "loader-container");
  let ldsEllipsis = document.createElement("div");
  ldsEllipsis.setAttribute("class", "lds-ellipsis");
  let i = 4;
  while (i > 0) {
    ldsEllipsis.appendChild(document.createElement("div"));
    i--;
  }
  loaderContainer.appendChild(ldsEllipsis);
  return loaderContainer;
};

const noProductsAvailableTemplate = () => {
  let element = document.createElement("h1");
  element.style.display = "flex";
  element.style.order = "-1";
  element.style.alignSelf = "center";
  element.innerText = "!!!NO PRODUCTS AVAILABLE!!!";

  return element;
};

const errorTemplate = () => {
  let element = document.createElement("h1");
  element.setAttribute("id", "error");
  element.style.display = "flex";
  element.style.order = "-1";
  element.style.alignSelf = "center";
  element.style.justifyContent = "center";
  element.style.alignItems = "center";
  element.innerText = "!!!ERROR FETCHING DATA!!!";

  return element;
};

const goBackTemplate = () => {
  let element = document.createElement("div");
  element.setAttribute("id", "go-back");
  element.setAttribute("onclick", "resetState()");
  element.innerText = "<--Go Back";

  return element;
};

const singleProductViewTemplate = (item) => {
  let productElement = document.createElement("div");
  productElement.id = "product";

  let imageContainer = document.createElement("div");
  imageContainer.className = "image";
  let image = document.createElement("img");
  image.setAttribute("src", item.image);
  image.setAttribute("alt", "product-image");
  imageContainer.appendChild(image);

  let detailsContainer = document.createElement("div");
  detailsContainer.className = "details";

  let name = document.createElement("div");
  name.className = "name";
  name.innerText = item.title;
  let rating = document.createElement("div");
  rating.className = "rating";
  rating.innerText = `${item.rating.rate}(${item.rating.count}+)`;
  let cost = document.createElement("div");
  cost.className = "cost";
  cost.innerText = item.price;
  let description = document.createElement("div");
  description.className = "description";
  description.innerText = item.description;

  detailsContainer.appendChild(name);
  detailsContainer.appendChild(rating);
  detailsContainer.appendChild(cost);
  detailsContainer.appendChild(description);

  productElement.appendChild(imageContainer);
  productElement.appendChild(detailsContainer);

  return productElement;
};

const showAllProducts = (category = "") => {
  document.getElementById("products-showcase").innerHTML = "";
  document.getElementById("products-showcase").appendChild(loaderTemplate());
  const categoryComponent = category === "" ? "" : `category/${category}`;
  fetchData(`${productsEndpoint}/${categoryComponent}`)
    .then((data) => {
      document.getElementById("products-showcase").innerHTML = "";
      const fragment = new DocumentFragment();
      if (data.length === 0) {
        fragment.appendChild(noProductsAvailableTemplate());
        return;
      }
      fragment.appendChild(
        singleCategoryShowcaseTemplate(
          category === "" ? "All Products" : decodeURI(category).capitalCase()
        )
      );

      data.map((item) => {
        fragment
          .getElementById("single-category-catalogue")
          .appendChild(productCardTemplate(item));
      });

      document.getElementById("products-showcase").appendChild(fragment);
    })
    .catch((reject) => {
      console.log(reject);
      document.getElementById("products-showcase").innerHTML = "";
      document.getElementById("products-showcase").appendChild(errorTemplate());
    });
};

const categoryBarCardTemplate = (category) => {
  const categoryCard = document.createElement("div");
  categoryCard.className = "category-card";
  categoryCard.onclick = () =>
    showAllProducts(category.replace("'", "%27").replace(" ", "%20"));

  const categoryImage = document.createElement("div");
  categoryImage.className = "category-image";
  const image = document.createElement("img");
  image.src = "https://via.placeholder.com/100x100?text=Category+Logo";
  image.alt = "category-image";
  categoryImage.appendChild(image);

  const categoryName = document.createElement("div");
  categoryName.innerText = category.capitalCase();

  categoryCard.appendChild(categoryImage);
  categoryCard.appendChild(categoryName);

  return categoryCard;
};

const categoryShowcaseTemplate = (category) => {
  const categoryShowcase = document.createElement("div");
  categoryShowcase.className = "category-showcase";
  categoryShowcase.id = category.kebabCase() + "-showcase";

  const categoryDetails = document.createElement("div");
  categoryDetails.className = "category-details";
  categoryDetails.innerText = category.capitalCase();

  const categoryCatalogue = document.createElement("div");
  categoryCatalogue.className = "category-catalogue";
  categoryCatalogue.id = category.kebabCase();

  categoryShowcase.appendChild(categoryDetails);
  categoryShowcase.appendChild(categoryCatalogue);

  return categoryShowcase;
};

const singleCategoryShowcaseTemplate = (category) => {
  const singleCategoryShowcase = document.createElement("div");
  singleCategoryShowcase.className = "single-category-showcase";
  singleCategoryShowcase.id = category.kebabCase() + "-showcase";

  const singleCategoryDetails = document.createElement("div");
  singleCategoryDetails.id = "single-category-details";
  singleCategoryDetails.innerText = category.capitalCase();

  const singleCategoryCatalogue = document.createElement("div");
  singleCategoryCatalogue.id = "single-category-catalogue";
  singleCategoryCatalogue.className = "in-single-category-view";

  singleCategoryShowcase.appendChild(singleCategoryDetails);
  singleCategoryShowcase.appendChild(singleCategoryCatalogue);

  return singleCategoryShowcase;
};

const productCardTemplate = (item) => {
  const card = document.createElement("div");
  card.onclick = () => viewSingleProduct(item.id);
  card.className = "card";
  card.id = item.id;

  const productImage = document.createElement("div");
  productImage.className = "product-image";
  const image = document.createElement("img");
  image.src = item.image;
  image.alt = "product-image";
  productImage.appendChild(image);

  const rating = document.createElement("div");
  rating.className = "rating";
  rating.innerText = item.rating.rate + `(${item.rating.count}+)`;

  const cost = document.createElement("class");
  cost.className = "cost";
  cost.innerText = item.price;

  const itemName = document.createElement("div");
  itemName.className = "item-name";
  itemName.innerText = item.title;

  const itemDescription = document.createElement("div");
  itemDescription.className = "item-description";
  itemDescription.innerText = " " + item.description;

  card.appendChild(productImage);
  card.appendChild(rating);
  card.appendChild(cost);
  card.appendChild(itemName);
  card.appendChild(itemDescription);

  return card;
};

String.prototype.kebabCase = function () {
  return this.removeSpecialCharacters().replaceSpaceWithHyphen();
};

String.prototype.capitalCase = function () {
  return this.split(" ")
    .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
    .join(" ");
};

String.prototype.removeSpecialCharacters = function () {
  return this.replace(/[^\w\s]/g, "");
};

String.prototype.replaceSpaceWithHyphen = function () {
  return this.replace(" ", "-");
};

const generateCategoriesBar = () => {
  fetchData(categoriesEndpoint).then((data) => {
    const categoriesBar = document.getElementById("categories-bar");

    data.forEach((category) => {
      categoriesBar.appendChild(categoryBarCardTemplate(category));
    });
  });
};

const showAllCategories = () => {
  document.getElementById("products-showcase").innerHTML = "";
  document.getElementById("products-showcase").appendChild(loaderTemplate());
  fetchData(categoriesEndpoint)
    .then((categories) => {
      fetchData(productsEndpoint)
        .then((data) => {
          document.getElementById("products-showcase").innerHTML = "";

          const fragment = new DocumentFragment();
          categories.forEach((category) => {
            fragment.appendChild(categoryShowcaseTemplate(category));

            data
              .filter((item) => {
                return item.category === category;
              })
              .forEach((item) => {
                fragment
                  .getElementById(`${category.kebabCase()}`)
                  .appendChild(productCardTemplate(item));
              });
            document.getElementById("products-showcase").appendChild(fragment);
          });
        })
        .catch((reject) => {
          console.log(reject);
          document.getElementById("products-showcase").innerHTML = "";
          document
            .getElementById("products-showcase")
            .appendChild(errorTemplate());
        });
    })
    .catch((reject) => {
      console.log(reject);
      document.getElementById("products-showcase").innerHTML = "";
      document.getElementById("products-showcase").appendChild(errorTemplate());
    });
};

const viewSingleProduct = (productID) => {
  // document.getElementById("showcase").setAttribute("style", "display: none");
  document
    .getElementById("categories-bar")
    .setAttribute("style", "display: none");
  document
    .getElementById("products-showcase")
    .setAttribute("style", "display: none");
  document.querySelector("main").appendChild(goBackTemplate());
  document.querySelector("main").appendChild(loaderTemplate());

  fetchData(`${productsEndpoint}/${productID}`)
    .then((item) => {
      document.querySelector(".loader-container").remove();
      document
        .querySelector("main")
        .appendChild(singleProductViewTemplate(item));
    })
    .catch((reject) => {
      console.log(reject);
      document.querySelector(".loader-container").remove();
      document.getElementById("products-showcase").appendChild(errorTemplate());
    });
};

const resetState = () => {
  // document.getElementById("showcase").setAttribute("style", "display: flex");
  document
    .getElementById("categories-bar")
    .setAttribute("style", "display: flex");
  document
    .getElementById("products-showcase")
    .setAttribute("style", "display: flex");
  try {
    document.getElementById("error").remove();
  } catch (err) {}
  document.getElementById("go-back").remove();
  document.getElementById("product").remove();
};

const startState = () => {
  generateCategoriesBar();
  showAllProducts();
};

startState();
